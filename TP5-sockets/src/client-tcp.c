#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/socket.h>
#include <string.h>

void displayMenu()
{
    printf("MENU :\n");
    printf("\t1) Length\n");
    printf("\t2) Sort\n");
    printf("\t3) Quit\n");
}

char getChoice()
{
    char choice;
    displayMenu();
    do
    {
        printf("Choice : ");
        scanf("%c%*c", &choice);
    
    } while ('3' < choice || choice < '1');
    
    return choice;
}


void loop(char *hostname, int port) 
{   
    char buffer[200],texte[200];   
    int rc, sock,i,c;  
    struct sockaddr_in addr;   
    struct hostent *entree;   

    addr.sin_port=htons(port);   
    addr.sin_family=AF_INET;   
    entree=(struct hostent *)gethostbyname(hostname);   
    bcopy((char *)entree->h_addr,(char *)&addr.sin_addr,entree->h_length);   
    sock= socket(AF_INET,SOCK_STREAM,0);   

    if (connect(sock, (struct sockaddr *)&addr,sizeof(struct sockaddr_in)) < 0) 
    {     
        perror("Error: Can't establish connection\n");
        perror("Please check the server is lauched\n");       
        exit(1); 
    }   

    char choice = getChoice();
    
    while (choice != '3') 
    {       
        bzero(texte,sizeof(texte));        
        bzero(buffer,sizeof(buffer));

        printf("Write your word : \n");
        scanf("%s%*c", texte);
        // strcat(texte,itoa(choice));

        texte[strlen(texte)] = choice;
        texte[strlen(texte)+1] = '\0';
        // strcpy(texte,"bonjour");

        printf("%s\n", texte);
        
        // printf(stdout, "%d %s\n", choice, texte);

        send(sock,texte,strlen(texte),0);       
        recv(sock,buffer,sizeof(buffer),0);       
        
        printf("recu %s\n",buffer);       
        if (strcmp("FIN",texte) == 0) 
            break;     

        choice = getChoice();
    }
    printf("Disconnecting...\n");
    close(sock);
}


int main(int argc, char const *argv[]) {

	if (argc !=3)  
    {
        printf("usage : [hostname] [port]\n"); 
        exit(1);    
    }   

	loop(argv[1], atoi(argv[2]));

	return 0;
}
