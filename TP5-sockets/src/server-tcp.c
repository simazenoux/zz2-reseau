#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/socket.h>
#include <strings.h>
#include <string.h> 


void swap(char*a, char* b)
{
    char tmp;
    tmp = *a;
    *a = *b;
    *b=tmp;
}

char* stringSort(char* string)
{
    int i, j;
    int n = strlen(string);
    for (i = 0; i < n-1; i++) {
        for (j = i+1; j < n; j++) {
            if (string[i] > string[j]) {
                swap(&string[i], &string[j]);
            }
        }
    }
    return string;
}





int main(int argc, char *argv[])   
{   
    int s_ecoute,scom, lg_app,i,j;  
    struct sockaddr_in adr;   
    struct sockaddr_storage recep;   
    char buf[1500], renvoi[1500], host[1024],service[20]; 

    if (argc != 2)  
    {       
        printf("usage : port\n"); 
        exit(1);    
    }  

    s_ecoute=socket(AF_INET,SOCK_STREAM,0);    
    printf("creation socket\n");    
    adr.sin_family=AF_INET;    
    adr.sin_port=htons(atoi(argv[1]));    
    adr.sin_addr.s_addr=INADDR_ANY;    
    if (bind(s_ecoute,(struct sockaddr *)&adr,sizeof(struct sockaddr_in)) !=0)
    {      
        printf("probleme de bind sur v4\n");      
        exit(1); 
    }    
    if (listen(s_ecoute,5) != 0)      
    {        
        printf("pb ecoute\n"); 
        exit(1);
    }
    printf("en attente de connexion\n");    
    while (1)  
    {       
        scom=accept(s_ecoute,(struct sockaddr *)&recep, (unsigned long *)&lg_app);       
        getnameinfo((struct sockaddr *)&recep,sizeof (recep), host, sizeof(host),service, sizeof(service),0);        
        printf("recu de %s venant du port %s\n",host, service);   
        while (1)     
        {        
            recv(scom,buf,sizeof(buf),0);        
            printf("buf recu %s\n",buf);
            bzero(renvoi,sizeof(renvoi));  
           
            if (buf[strlen(buf)-1]=='1')
            {
                // printf("Length : %d\n", strlen(renvoi)-1); 
                sprintf(renvoi, "%d", strlen(buf)-1); 
            }
            else if (buf[strlen(buf)-1]=='2')
            {
                buf[strlen(buf)-1] = '\0';
                printf("%s\n",stringSort(buf));
                strcpy(renvoi, buf);
            }
            else
            {
                strcpy(renvoi, "Operation not supported");
            }
       
            send(scom,renvoi,strlen(renvoi),0);        
            bzero(buf,sizeof(buf));        
            if (strcmp(renvoi,"NIF") == 0)    
                break;   
            }   
            close(scom);   
        } 
    close(s_ecoute); 
}