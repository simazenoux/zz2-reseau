# TP5 : Socket

## Objectif
L'objectif du TP est de créer un client et un serveur afin de comprendre les notions de ports et de communication réseau

## Compilation
Pour compiler, il vous faut les packets make et gcc. Pour compiler le code il suffit d'effectuer la commande make :
```bash
make
```
## Lancement
0- Vérifiez que vous ayez bien compilé le code (voir section compilation).

1- Choisir un port libre (entre 1024 et 65000)
```bash
PORT=7070
```
2- Lancer le serveur
```bash
./out/server-tcp $PORT
```
3- Lancer le client
```bash
./out/client-tcp $HOSTNAME $PORT
```